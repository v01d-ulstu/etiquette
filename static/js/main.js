for (navpoint of navitems.children) {
    if (navpoint.getElementsByTagName('a')[0].href != location.href)
        continue;

    navpoint.classList.add('active');
    break;
}

$(function () {
    $("#ButtonTop").click(function () {
        $('html, body').animate({
            scrollTop: $("#header").offset().top
        }, 1000);
        return false;
    });
});

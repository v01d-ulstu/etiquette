from sqlalchemy import Column, Integer, String, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Task(Base):
    __tablename__ = 'tasks'
    id = Column(Integer, primary_key=True)
    reference = Column(String, nullable=False)
    task = Column(String, nullable=False)
    answer = Column(String, nullable=False)
    description = Column(String)

    def asdict(self):
        return {
            'id': self.id,
            'reference': self.reference,
            'task': self.task,
            'answer': self.answer,
            'description': self.description,
        }


class TestResult(Base):
    __tablename__ = 'test_results'
    id = Column(Integer, primary_key=True)
    percent = Column(Float, nullable=False)
    time_took = Column(Float, nullable=False)
    time_finish = Column(DateTime, nullable=False)
    author = Column(String)

    def asdict(self):
        return {
            'percent': self.percent,
            'time_took': self.time_took,
            'time_finish': self.time_finish,
            'author': self.author,
        }

from os.path import join
from os import getcwd


CWD = getcwd()
DATABASE_FILE = join(CWD, 'database.sqlite')
SECRET_KEY = b'QKmIhX7v0xCT55gDzbpJSziUYFXFWJRB'
LOGFILE = join(CWD, 'log.txt')
LOGGING_FORMAT = '[%(asctime)s UTC] %(levelname)s from %(name)s: %(message)s'
DATEFORMAT = '%d.%m.%Y, %H:%M:%S'
TEMPLATE_FOLDER = join(CWD, 'templates')
STATIC_FOLDER = join(CWD, 'static')
MASTER_PASSWORD = 'v01dtophvhi'

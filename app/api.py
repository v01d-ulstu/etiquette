from json import loads
from datetime import datetime

from flask import Blueprint, request, abort, jsonify
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound

from app.config import MASTER_PASSWORD
from app.utils import base_json_validation_check
from app.database.core import DBConnection
from app.database.models import *


api = Blueprint('api', __name__)


@api.route('/')
def api_index():
    return 'Welcome to API'


@api.route('/get-task')
def send_task():
    db = DBConnection()

    try:
        task_id = request.args['id'].strip()
    except KeyError:
        abort(400)
    try:
        task = db.query(Task).filter(Task.id == task_id).one()
    except NoResultFound:
        abort(404)

    return jsonify(task.asdict())


@api.route('/get-tasks')
def send_tasks():
    db = DBConnection()

    try:
        reference = request.args['reference'].strip()
    except KeyError:
        abort(400)
    try:
        count = int(request.args['count'])
    except (KeyError, ValueError):
        count = 10

    tasks = db.query(Task).filter(Task.reference == reference).order_by(func.random()).limit(count).all()

    return jsonify([{
        'id': task.id,
        'task': task.task,
        'description': task.description,
    } for task in tasks])


@api.route('/check-tasks', methods=('POST',))
def check_tasks():
    db = DBConnection()

    try:
        answers_json = request.form['answers'].strip()
        time_took = float(request.form['time_took'])
        author_name = request.form.get('author')
    except (KeyError, ValueError):
        abort(400)

    if author_name is not None:
        author_name = author_name.strip()

    if not base_json_validation_check(answers_json, list):
        abort(400)

    user_answers = loads(answers_json)
    wrong_answers = {}

    for task in user_answers:
        try:
            task_id = task['id'].strip()
            user_answer = task['answer'].strip()
        except KeyError:
            abort(400)
        try:
            answer = db.query(Task).filter(Task.id == task_id).one().answer
        except NoResultFound:
            abort(404)

        if answer != user_answer:
            wrong_answers[str(task_id)] = answer

    result = TestResult(
        percent=round((1 - len(wrong_answers) / len(user_answers)) * 100, 2),
        time_took=time_took,
        time_finish=datetime.utcnow(),
        author=author_name,
    )
    db.add(result)
    db.commit()

    return jsonify(wrong_answers)


@api.route('/get-all-references')
def send_all_references():
    db = DBConnection()
    return jsonify([row[0] for row in db.query(Task.reference).distinct().all()])


@api.route('/add-task', methods=('POST',))
def add_task():
    try:
        password = request.form['password'].strip()
        reference = request.form['reference'].strip()
        task = request.form['task'].strip()
        answer = request.form['answer'].strip()
        description = request.form.get('description').strip()
    except KeyError:
        abort(400)

    if password != MASTER_PASSWORD:
        abort(403)

    db = DBConnection()
    task = Task(reference=reference, task=task, answer=answer, description=description)
    db.add(task)
    db.commit()

    return 'OK'


@api.route('/clear-top')
def clear_top():
    try:
       password = request.args['password'].strip()
    except KeyError:
        abort(400)

    if password != MASTER_PASSWORD:
        abort(403)

    db = DBConnection()
    db.query(TestResult).delete()
    db.commit()

    return 'OK'


@api.route('/delete-test-result')
def delete_test_result():
    try:
       password = request.args['password'].strip()
       result_id = int(request.args['id'])
    except (KeyError, ValueError):
        abort(400)

    if password != MASTER_PASSWORD:
        abort(403)

    db = DBConnection()
    db.query(TestResult).filter(TestResult.id == result_id).delete()
    db.commit()

    return 'OK'

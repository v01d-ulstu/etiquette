from flask import Flask

from app.config import SECRET_KEY, TEMPLATE_FOLDER, STATIC_FOLDER
from app.api import api
from app.views import views


app = Flask(__name__, template_folder=TEMPLATE_FOLDER, static_folder=STATIC_FOLDER)
app.secret_key = SECRET_KEY
app.register_blueprint(api, url_prefix='/api/')
app.register_blueprint(views)

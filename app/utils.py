from datetime import datetime, timedelta
from json import loads
from json.decoder import JSONDecodeError


timedelta_gmt4 = timedelta(hours=4)


def servertime() -> datetime:
    return datetime.utcnow()


def localize_fromutc(utc_time: datetime) -> datetime:
    return utc_time + timedelta_gmt4


def base_json_validation_check(json_sting, check_type=None):
    """
    :param json_sting:
    :param check_type:
    :return: True if json_string is a valid JSON else False
    """

    try:
        parsed_json = loads(json_sting)
    except JSONDecodeError:
        return False

    if check_type is not None and not isinstance(parsed_json, check_type):
        return False

    return True
